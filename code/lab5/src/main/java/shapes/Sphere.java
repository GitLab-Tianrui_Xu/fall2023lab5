package shapes;

public class Sphere {
   /**
    * @Param doublradius 
    *    
    */
    private double radius;
    public Sphere(double radius){
        if(radius < 0) {
            throw new IllegalArgumentException();
        }
        this.radius=radius;
    }
    /**
      @return double this.volume;
     
     **/
    public double getVolume(){
        
        return 4.0 / 3.0 * Math.PI * Math.pow(this.radius, 2.0);
    }
    
    /**
     * @return  this.radius;
     */
    public double getRadius(){
       return this.radius;
    }
    /**
     * @param double this.radius;
     */
    public void setRadius(double radius){
        if(radius < 0) {
            throw new IllegalArgumentException();
        }
        this.radius=radius;
    }
    /**
     * @return doublesurfacearea
     */
    public double getSurfaceArea(){
      return  4 * Math.PI * Math.pow(this.radius, 2);
    }
    /**
     * @return radius,volume,surfacearea
     */
   public String toString(){
        return "Radius: " + this.radius + ", Volume: " + this.getVolume() + ", Surface Area: " + this.getSurfaceArea();
   }


}
