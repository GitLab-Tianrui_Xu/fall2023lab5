package shapes;

public class Cylinder implements Shape3d{
    private double radius;
    private double height;

    /**
     * method initialize fields radius and height
     * @param double radius
     * @param double height
     */
    public Cylinder(double radius, double height){
        if(radius < 0 || height < 0) {
            throw new IllegalArgumentException();
        }
        this.radius = radius;
        this.height = height;
    }
    /**
     * method return this.radius
     * @return this.radius
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * method return this.height
     * @return this.height
     */
    public double getHeight(){
        return this.height;
    }

    /**
     * method set this.radius
     * @param double radius
     */
    public void setRadius(double radius){
        if(radius < 0) {
            throw new IllegalArgumentException();
        }
        this.radius = radius;
    }

    /**
     * method set this.height
     * @param double height
     */
    public void setHeight(double height){
        if(height < 0) {
            throw new IllegalArgumentException();
        }
        this.height = height;
    }

    /**
     * method return the volume
     * @return the volume of the cylinder
     */
    public double getVolume(){
        return Math.pow(this.radius, 2) * Math.PI * this.height;
    }

    /**
     * method return the surface area
     * @return the surface area of the cylinder
     */
    public double getSurfaceArea(){
        return 2.0 * Math.PI * this.radius * this.height + 2.0 * Math.PI * Math.pow(this.radius, 2);
    }

    /**
     * method override toString
     * @return this.radius, this.height, volume, surface area
     */
    public String toString(){
        return "Radius: " + this.radius + ", Height: " + this.height + ", Volume: " + this.getVolume() + ", Surface Area: " + this.getSurfaceArea();
    }
}
