/**
 * @author Tianrui Xu
 */
package shapes;
import static org.junit.Assert.*;
import org.junit.Test;
public class SphereTest {
    private final double delta = 0.0001;
    @Test (expected = IllegalArgumentException.class)
    public void constructorTest() {
        Sphere s = new Sphere(-2);
    }

    @Test
    public void getRadiusTest() {
        Sphere s = new Sphere(2);
        assertEquals(2, s.getRadius(), delta);
    }

    @Test
    public void setRadiusTest() {
        Sphere s = new Sphere(2);
        s.setRadius(3);
        assertEquals(3, s.getRadius(), delta);
    }

    @Test (expected = IllegalArgumentException.class)
    public void setRadiusExceptionTest() {
        Sphere s = new Sphere(2);
        s.setRadius(-2);
    }

    @Test
    public void getVolumeTest() {
        Sphere s = new Sphere(3);
        assertEquals(4.0 / 3.0 * Math.PI * Math.pow(3.0, 2.0), s.getVolume(), delta);
    }

    @Test
    public void getSurfaceAreaTest() {
        Sphere s = new Sphere(4);
        assertEquals(4 * Math.PI * Math.pow(4, 2), s.getSurfaceArea(), delta);
    }

    @Test 
    public void toStringTest() {
        Sphere s = new Sphere(4);
        assertEquals(s.toString(),("Radius: " + s.getRadius() + ", Volume: " + s.getVolume() + ", Surface Area: " + s.getSurfaceArea()));
    }
}
