/**
 @author Hasani Freeman
 */
package shapes;
import static org.junit.Assert.*;
import org.junit.Test;

public class CylinderTest {
private final double delta=0.0001;
@Test (expected =IllegalArgumentException.class)
public void Constructor(){
    Cylinder c = new Cylinder(-2,1);
}

@Test (expected =IllegalArgumentException.class)
public void Constructor1(){
    Cylinder d = new Cylinder(-2,-5);
}

@Test (expected =IllegalArgumentException.class)
public void Constructor3(){
    Cylinder h = new Cylinder(2,-3);
}

@Test (expected =IllegalArgumentException.class)
public void setRadiusTest(){
    Cylinder c= new Cylinder(2, 7);
    c.setRadius(-3);
}

@Test (expected =IllegalArgumentException.class)
public void setHeightTest(){
    Cylinder c= new Cylinder(2, 7);
    c.setHeight(-3);
}
@Test
    public void setRadiusTest2() {
        Cylinder s = new Cylinder (2,5);
        s.setRadius(3);
        assertEquals(3, s.getRadius(), delta);
    }

    @Test
    public void setHeight2() {
        Cylinder s = new Cylinder (2,5);
        s.setHeight(3);
        assertEquals(3, s.getHeight(), delta);
    }
    
    @Test
    public void getvolumeTest(){
        Cylinder s = new Cylinder (2,5);
        assertEquals(Math.pow(2, 2) * Math.PI * 5, s.getVolume(), delta);
    }

    @Test
    public void getSurfaceAreaTest(){
        Cylinder s = new Cylinder (2,5);
        assertEquals(2.0 * Math.PI * 2 * 5 + 2.0 * Math.PI * Math.pow(2, 2), s.getSurfaceArea(), delta);
    }

    @Test
    public void getHeightTest(){
        Cylinder s = new Cylinder (2,5);
        assertEquals(5, s.getHeight(), delta);
    }


    @Test 
    public void toStringTest() {
        Cylinder s = new Cylinder(4,5);
        assertEquals(s.toString(), "Radius: " + s.getRadius() + ", Height: " + s.getHeight() + ", Volume: " + s.getVolume() + ", Surface Area: " + s.getSurfaceArea());
    }
}


